import java.util.Scanner;

    public class M5 {
        private Scanner sc = new Scanner(System.in);

        public void showMenu() {
            System.out.println("Escoge una opcion:");
            System.out.println("1.Hello World");
            System.out.println("2.Find Max");
            System.out.println("3.Find Min");
            System.out.println("0. Salir\n");
        }

        public int readOpcion() {
            return sc.nextInt();
        }

        public void operar(int opcion) {

            switch (opcion) {
                case 0:
                    exit();
                    break;
                case 1:
                    helloWorld();
                    break;
                case 2:
                    findMax_hjsb();
                    break;
                case 3:
                    findMin_hjsb();
                    break;
            }
        }

        /* OPERATIONS */
        public void exit() {
            System.out.println("END");
        }

        public void helloWorld() {
            System.out.println("Hello World");
        }

        public void findMax_hjsb(){
            int numero=-1;
            int max = Integer.MIN_VALUE;
            while(numero!=0){
                System.out.println("Introduce number");
                numero= sc.nextInt();
                if(numero > max) max=numero;
                }
            System.out.println("The maxim is" + max);
            System.out.println();
        }

        public void findMin_hjsb() {
            int numero = -1;
            int min = Integer.MAX_VALUE;
            while (numero != 0) {
                System.out.println("Introduce number");
                numero = sc.nextInt();
                if (numero < min) min = numero;
            }
            System.out.println("The maxim is " + min);
            System.out.println();
        }


        public static void main(String[] args) {

            M5 p5 = new M5();

            int response = 0;
            do {
                p5.showMenu();
                response = p5.readOpcion();
                p5.operar(response);
            } while (response != 0);


        }
    }



